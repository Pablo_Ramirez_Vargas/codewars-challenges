#https://www.codewars.com/kata/5277c8a221e209d3f6000b56
def validBraces(string):
    a="([{}])"
    g=0
    e=[]
    for s in string:
        for pos,iteam in enumerate(a):
            if iteam in s and g<0:
                return False
            elif (pos>=(len(a)//2))and iteam in s:
                g+=(pos-len(a)+1)
            elif (pos<len(a)//2) and iteam in s:
                g+=(pos)
    return g==0
#https://www.codewars.com/kata/513e08acc600c94f01000001
def rgb(r, g, b):
    w=""
    for e,iteam in enumerate((r,g,b)):
        if iteam>255:
            w+="FF"
        elif iteam>16:
            w+=(hex(iteam)[2::]).upper()
        elif iteam>=0:
            w+=("0"+hex(iteam)[2::]).upper()
        else:
             w+=("00")

    return w
#https://www.codewars.com/kata/525c65e51bf619685c000059
def cakes(recipe, available):
    e=[]
    for pos,iteam in enumerate(recipe):
        if not (iteam in available):
            return 0
        else:
            e.append(available[iteam]/recipe[iteam])
    return min(e)
#https://www.codewars.com/kata/529b418d533b76924600085d
def to_underscore(string):
    if not(type(string) is str):
        return str(string)
    else:
        e=[]
        for position,iteam in enumerate(string):
            if iteam.isupper()==True:
                if position>0:
                    e.append("_"+iteam.lower())
                else:
                    e.append(iteam.lower())
            else:
                e.append(iteam)
        return "".join(e)
#https://www.codewars.com/kata/5268acac0d3f019add000203
class Automaton(object):

    def __init__(self):
        self.states = []

    def read_commands(self, commands):
        self.e=1
        self.que=[]
        self.commands=commands
        for possition,iteam in enumerate(self.commands):
            if(iteam=="1" and self.e==1):
                self.e=2
                self.que.append("2")
                continue
            elif(iteam=="0" and self.e==2):
                self.e=3
                self.que.append("3")
                continue
            elif(self.e==3):
                 self.e=2
                 self.que.append("2")
        # Return True if we end in our accept state, False otherwise
        return self.que[len(self.que)-1]=="2"
my_automaton = Automaton()
#https://www.codewars.com/kata/525f039017c7cd0e1a000a26
def palindrome_chain_length(n):
    c=0
    while(True):
        if int(str(n)[::-1])==n:
            break
        else:
            n+=int(str(n)[::-1])
            c+=1
    return c
#https://www.codewars.com/kata/552c028c030765286c00007d
def iq_test(numbers):
    numbers=map(lambda x:int(x),numbers.split(" "))
    odd=filter(lambda x:x%2!=0 or x==1,numbers)
    even=filter(lambda x:x%2==0,numbers)
    if(len(odd)==1):
        return numbers.index(odd[0])+1
    else:
        return numbers.index(even[0])+1
#https://www.codewars.com/kata/526dad7f8c0eb5c4640000a4
from math import sqrt
class Vector:
    def __init__(self,a):
        self.a=a
    def add(self,b):
        self.result=[]
        self.b=b.a
        try:
            for i in range(len(self.b)):
                self.result.append(self.a[i]+self.b[i])
        except ValueError:
            return "error"
        return Vector(self.result)
    def subtract(self,c):
        self.result=[]
        self.c=c.a
        try:
            for i in range(len(self.c)):
                self.result.append(self.a[i]-self.c[i])
        except ValueError:
            return "error"
        return Vector(self.result)
    def dot(self,d):
        self.d=d.a
        self.result2=0
        try:
            for i in range(len(self.d)):
                self.result2+=(self.d[i]*self.a[i])
        except ValueError:
            return "error"
        return self.result2
    def norm(self):
        return sqrt(sum(map(lambda x: x**2,self.a)))
    def equals(self,e):
        self.e=e.a
        if sorted(self.e)==sorted(self.a):
            return True
        else:
            return False
    def __str__(self):
        self.reee="("
        for i in range(len(self.a)):
            self.reee+=str(self.a[i])
            if i!=len(self.a)-1:
                self.reee+=","
        self.reee+=")"
        return self.reee
#https://www.codewars.com/kata/5418a1dd6d8216e18a0012b2
def validate(n):
    n=list(str(n))
    r=[]
    if len(n)%2==0:
        for i in range(0,len(n),2):
            if i!=0:
                r.append(int(n[i-1]))
            if int(n[i])*2>=9:
                r.append((int(n[i])*2)-9)
            else:
                r.append((int(n[i])*2))
    else:
        for i in range(1,len(n),2):
            r.append(int(n[i-1]))
            if int((n[i])*2)>=9:
                r.append((int(n[i])*2)-9)
            else:
                r.append((int(n[i])*2))
    r.append(int(n[len(n)-1]))
    return sum(r)%10==0
#https://www.codewars.com/kata/54dc6f5a224c26032800005c
def stock_list(listOfArt, listOfCat):
    final=""
    r=0
    def digits(e):
        r=""
        for n in range(len(e)):
            if e[n].isdigit()==True:
                r+=e[n]
        return int(r)
    for count in range(len(listOfCat)):
        numbers=0
        for count2 in range(len(listOfArt)):
            if listOfCat[count]==list(listOfArt[count2])[0]:
                  numbers+=digits(listOfArt[count2])
                  if numbers==0:
                      r+1
        final+="("+listOfCat[count]+" : "+str(numbers)+")"
        if(count!=len(listOfCat)-1):
            final+=" - "
    if r!=len(listOfArt):
        return final
    else:
        return ""
#https://www.codewars.com/kata/52774a314c2333f0a7000688
def valid_parentheses(string):
    if string.count("(")==string.count(")"): #is like a light shwitch :D
        control=0
        for i in range(len(string)):
            if string[i]=="(":
                control+=1
            if string[i]==")":
                control-=1
            if control<0:
                return False
        return control==0
    else:
        return False
#https://www.codewars.com/kata/525f47c79f2f25a4db000025
def validPhoneNumber(phoneNumber):
    phoneNumber=list(phoneNumber)
    if phoneNumber[0]=="(" and phoneNumber[4]==")"and phoneNumber[5]==" "and phoneNumber[9]=="-" and len(phoneNumber)==14:
        phoneNumber.remove("(")
        phoneNumber.remove(")")
        phoneNumber.remove(" ")
        phoneNumber.remove("-")
        for i in range(len(phoneNumber)):
            if phoneNumber[i].isdigit==False:
                return False
        return True
    return False
def main():
    print("ve las funciones de arriva y modifica el codigo para ver que mi codigo funciona")
    #ejemplo
    print (valid_parentheses("[][()]"))
    v=input("")
main()
